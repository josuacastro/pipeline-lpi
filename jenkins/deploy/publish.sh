#!/bin/bash

export REGISTRY="192.168.1.76:5043"
export IMAGE=$(sed -n '1p' /tmp/.auth)
export TAG=$(sed -n '2p' /tmp/.auth)

cd ~/jenkins/ && docker-compose up -d
