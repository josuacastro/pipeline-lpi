#!/bin/bash

# Generamos
echo app > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth

# Transfiriendo archivos

scp /tmp/.auth root@192.168.1.76:/tmp/.auth
scp jenkins/deploy/publish.sh root@192.168.1.76:/tmp/publish
ssh root@192.168.1.76 /tmp/publish
